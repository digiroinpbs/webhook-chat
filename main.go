package main

import (
	"net/http"
	"encoding/json"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"strings"
)

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func getParameter()map[string]string{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	username:=p.GetString("twilio.sid","AC8e0e5aa72d449f96747198b3aaa4da44")
	password:=p.GetString("twilio.key","0e978a864b0a468c9f94111f6b4a07de")
	timeout:=p.GetString("twilio.timeout","5")
	from:=p.GetString("twilio.from","6285574679971")
	return map[string]string{
		"Username": username,
		"Password": password,
		"Timeout": timeout,
		"From": from,
	}
}

type Chat struct {
	PublicAddress string
	Message string
}

func main() {
	http.HandleFunc("/digiroin/webhook/chat", balance)
	http.ListenAndServe(":7065", nil)
}


//contoh input
//{
//"PublicAddress":"6281213631232",
//"Message":""
//}
func balance(w http.ResponseWriter, r *http.Request) {
	req := Chat{}
	err := json.NewDecoder(r.Body).Decode(&req)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
		go info("Error","invalid json parameter","400")

	}else{
		if(req.PublicAddress=="" || req.Message==""){
			w.WriteHeader(http.StatusBadRequest)
			result = `{"Error":"insert paramater public address and message"}`
			go info("Error","insert paramater public address and message","400")
		}else{
			w.WriteHeader(http.StatusOK)
			result=`{"status":"success"}`
			go info("Info",`from `+req.PublicAddress+` message `+req.Message,"200")
		}
	}
	w.Write([]byte(result))
}

func info(typeInfo string, message string, code string){
	body := strings.NewReader(`{ "type" :"`+typeInfo+`" ,"message" : "`+message+`","code" : "`+code+`" }`)
	req, err := http.NewRequest("POST", "http://logs-01.loggly.com/inputs/5d973c32-dcab-46d9-83f1-85f7a0000ed9/tag/webhook-chat/", body)
	if err != nil {
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
	}
	defer resp.Body.Close()
}